package Assignment;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;



public class TimeConversion {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enetr 12 hour format time");
		String s= sc.nextLine();
		DateFormat inFormat=new SimpleDateFormat("hh:mm:ssaa");
		DateFormat OutFormat =new SimpleDateFormat("HH:mm:ss");
		
		Date date=null;
		try {
			date=inFormat.parse(s);
		}
		catch(ParseException e) {
			e.printStackTrace();
		}
		
	        if(date!=null) {
	        	String myDate=OutFormat.format(date);
	        	System.out.println(myDate);
	        }
	        sc.close();
	}
	
	}
	