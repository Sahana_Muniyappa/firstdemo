package Assignment2;
import java.util.*;



   public class Array
    {
        public static void main(String[] args)
        {
            ArrayList<String> list = new ArrayList<String>();
        
            //insert 3 elements
            list.add("10");
            list.add("20");
            list.add("30");
            
            //print the elements
            System.out.println("The total elements in the list are:");
            System.out.println(list.size());
            
            //Iterator has next element,it will display the element until we reach the end of the list .
            System.out.println("While Loop result:");
            Iterator<String> iterate = list.iterator();
            while(iterate.hasNext())
            {
                System.out.println(iterate.next());
            }
            
            //create an object for the list then print object
        /*    System.out.println("Its Advanced Loop:");
            for(Object obj : list)
            {
            System.out.println(obj);
        //    } */
            
            //for loop until ArrayList limit or size is reached.
            System.out.println("For Loop result:");
            for(int i=0; i<list.size(); i++)
            {
                
                // print each element using get method for each iteration
                System.out.println(list.get(i));
            }
        }
    }