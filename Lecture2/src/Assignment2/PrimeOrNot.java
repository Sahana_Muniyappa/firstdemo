package Assignment2;
import java.util.Scanner;

public class PrimeOrNot 
{
	  public static void main(String[] args)
	    {
	        
	        //declare temp for remainder for each iteration and num
	        int temp, num;
	        
	        //if isPrime is 0,then it will be False
	        boolean isPrime = true;
	        Scanner in =new Scanner(System.in);
	        System.out.println("Enter number");
	        num = in.nextInt();
	        
	        //looping from 2
	        for(int i=2; i<= num/2; i++)
	        {
	            temp= num % 1;
	            if ( temp == 0)
	            {
	                isPrime = false;
	                break;
	                
	            }
	        }
	        
	        if(isPrime)
	            
	            System.out.println("Number is Prime");
	        else
	            System.out.println("Number is notPrime");
	            in.close();
	    }
	}

