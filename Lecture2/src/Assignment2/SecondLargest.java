package Assignment2;



public class SecondLargest
{



   public static void main(String[] args)
    {
        //initialize an array for random numbers
        int arr[] = {16, 18, 66, 67, 90, 24, 52, 10, 377, 34, 60 };
        
        //largest and second largest integers.
        int largest = 0;
        int slargest = 0;
        
        //Print array
        System.out.println("The given array is:");
        
        for (int i = 0; i < arr.length; i++)
        {
            System.out.print(arr[i] + "\t");
            }
        
        for (int i = 0; i < arr.length; i++)
        {
            
            if (arr[i] > largest)
            {
                slargest = largest;
                largest = arr[i];
                }
            
            else if (arr[i] > slargest)
            {
                slargest = arr[i];
            }
        }
        
        System.out.println("\nLargest Number is: " +largest);
        System.out.println("Second largest number is:" + slargest);
        
    }
}