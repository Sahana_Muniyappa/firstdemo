package Assignment2;



import java.util.Scanner;



public class SwapNumbers
{



   public static void main(String[] args)
    {
        
        //x , y  values with temp variable
        int x,y,temp;
        System.out.println("Enter X and Y");
        Scanner in = new Scanner(System.in);
    
        
        //nextInt: for input of x and y
        x=in.nextInt();
        y=in.nextInt();
        
        //assign 3rd variable
        temp=x;
        x=y;
        y=temp;
        System.out.println("After Swap X and Y is : " +x+ " and " +y);
        in.close();
    }
}