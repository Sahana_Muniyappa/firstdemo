package collections;

import java.util.ArrayList;
import java.util.List;

public class ListExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       List<String> list = new ArrayList<>();
        list.add("Mango");
        list.add("apple");
        list.add("Banana");
        list.add("Grapes");
        System.out.println(list);
	}

}
