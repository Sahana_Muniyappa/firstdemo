package collections;

import java.util.LinkedList;
import java.util.Queue;

public class PeekAndPoll {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Queue<Integer> numbers = new LinkedList();
        numbers.offer(101);
        numbers.offer(101);
        numbers.offer(101);
        numbers.offer(101);
        numbers.offer(101);
        numbers.offer(101);
        System.out.println(numbers);
        //peak method
        int somename=numbers.peek();
        System.out.println("head of the queue is:" +somename);
        //poll name
        int removednumber = numbers.poll();
        System.out.println("head of queue is:" +removednumber);
        System.out.println(numbers);
	}

}
