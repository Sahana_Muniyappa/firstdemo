package collections;
/**
public class SmallestInArray {
	public static void main(String[] args) {
        int[] numbers = {88,1,55,0,64,123};
        int smallest = Integer.MAX_VALUE;
         
        for(int i =0;i<numbers.length;i++) {
            if(smallest > numbers[i]) {
                smallest = numbers[i];
            }
        }
         
        System.out.println("Smallest number in array is : " +smallest);
	}

}
**/
/**import java.util.Arrays;
public class SmallestInArray {
	
	public static void main(String[] args) {
		int num[]= {40, 50, 60,20, 30};
		Arrays.sort(num);
		System.out.println("the sallest array:" +num[0]);
	}
}
**/
import java.util.Scanner;

public class SmallestInArray
{

	public static void main(String[] args) 
	{
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter the number of elements in an array");
		int min;
		int n= scan.nextInt();
		int[] arr=new int[n];
		for(int i=0;i<n;i++)
			{
			  System.out.print("Enter the element "+(i+1)+": ");
			  arr[i]=scan.nextInt();
			}
		min=arr[0];
		for(int i=0;i<n;i++)
		{
			if(min>arr[i])
			{
				min=arr[i];
			}
		}
		System.out.println("\nThe smallest Elements is:"+min);
	}}
