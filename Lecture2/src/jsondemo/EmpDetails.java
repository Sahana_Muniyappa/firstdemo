package jsondemo;



public class EmpDetails
{
    public String name;
    public int age;
    public String city;
    private String gender;
    private String country;
    
    
        //getters and setters method
        public String getName()
        {
            return name;
        }
        public void setName(String name)
        {
            this.name= name;
        }
        public int getAge()
        {
            return age;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public String getCity()
        {
            return city;
        }
        public void setCity(String city)
        {
            this.city = city;
        }
        public void setgender(String gender)
        {
            this.gender= gender;
        }
        public void setCountry(String country)
        {
            this.country = country;
        }
        @Override
        public String toString()
        {
            return "EmpDetails [name=" + name + ", age=" + age + ", city=" + city + ", gender=" + gender + ", country=" + country+"]";
        }
        
    }


