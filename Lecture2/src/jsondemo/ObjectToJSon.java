package jsondemo;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
public class ObjectToJSon
{
    
        public static void main(String[] args)
        {
            Gson gson= new GsonBuilder().setPrettyPrinting().create();
            EmpDetails ed = createmyObject();
            System.out.println(ed); // java object printed
            String j = gson.toJson(ed); // to convert java object to JSON
            System.out.println(j);
            try(FileWriter writer = new FileWriter("C:\\Users\\241648\\git\\firstdemo\\Lecture2\\jsondemo.json"))
            
            {
                gson.toJson(ed, writer);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            }
        private static EmpDetails createmyObject()
        {
            EmpDetails ed= new EmpDetails();
            ed.setName("Sahana");
            ed.setAge(23);
            ed.setCity("Bangalore");
            ed.setgender("Female");
            ed.setCountry("India");
            return ed;
            }
}
