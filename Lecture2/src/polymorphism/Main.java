package polymorphism;

public class Main {
	public static void main(String[] args)  {
		Animal a=new Animal();
		Dogg d= new Dogg();
		Pig p = new Pig();
		d.sound(); //d- object, sound- method
		a.sound();
		p.sound();
	}

}
